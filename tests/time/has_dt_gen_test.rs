
#[cfg(test)]
mod tests {
	use moebius_tools::time::has_dt_gen::HasDtGen;
	use chrono::{DateTime, Utc};

	struct MockData{
		pub dt_gen: DateTime<Utc>
	}

	impl MockData {
		pub fn create(dt: DateTime<Utc>) -> Self {
			Self{dt_gen:dt}
		}
	}

	impl HasDtGen for MockData {
		fn get_dt_gen(&self) -> DateTime<Utc> {
			self.dt_gen
		}
	}

	#[test]
	fn has_the_trait() {
		let now = Utc::now();
		let mock = MockData::create(now.clone());
		assert_eq!(mock.get_dt_gen(),now)
	}

}
