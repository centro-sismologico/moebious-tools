
#[cfg(test)]
mod tests {
	use moebius_tools::time::dt_gen_format::{serialize, deserialize};
	use chrono::{DateTime, Utc};
	use serde::{Deserialize, Serialize};

	#[derive(Debug, Serialize, Deserialize, Clone)]
	struct MockData{
		#[serde(serialize_with = "serialize", deserialize_with = "deserialize")]
		pub dt_gen: DateTime<Utc>
	}

	impl MockData {
		pub fn create(dt: DateTime<Utc>) -> Self {
			Self{dt_gen:dt}
		}
	}

	/*
	test 1:
	Just create a date -> serialize and deserialize
	check

	test 2:
	Create wrong date, raise error
	 */

	#[test]
	fn try_serde() {
		let now = Utc::now();
		let mock = MockData::create(now);
		let serialized = serde_json::to_string(&mock).unwrap();
		let newmock: MockData = serde_json::from_str(&serialized).unwrap();
		assert_eq!(newmock.dt_gen,now)

	}



}
