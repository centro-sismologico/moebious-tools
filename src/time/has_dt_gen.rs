use chrono::{DateTime, Utc};

pub trait HasDtGen {
    fn get_dt_gen(&self) -> DateTime<Utc>;
}
