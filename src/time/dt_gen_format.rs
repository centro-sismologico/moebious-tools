use chrono::{DateTime, TimeZone, Utc};
use serde::{self, Deserialize, Deserializer, Serializer};

const FORMAT: &str = "%d/%m/%YT%H:%M:%S%.f%Z";

/// Format to serialize a DateTime<Utc> object
pub fn serialize<S>(date: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let s = format!("{}", date.format(FORMAT));
    serializer.serialize_str(&s)
}

/// Format to deserialize a DateTime<Utc> object
pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;

	// Use TimeZone here::
    Utc.datetime_from_str(&s, FORMAT)
        .map_err(serde::de::Error::custom)
}
